package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controller.Ctrl;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * Classe d�finissant la vue de recherche d'un rendez-vous
 * @author xavier
 *
 */
public class MeetingSearch extends JDialog implements ViewM2L{

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private static JTable tableMeet;
	
	/**
	 * M�thode permettant de mettre � jour le contenu de la table
	 * @param dataTable le contenu de la table
	 * @param columnsTable le nom des colonnes de la table
	 */
	public static void setTable(String[][] dataTable, String[] columnsTable){
		DefaultTableModel model = new DefaultTableModel(dataTable, columnsTable);
		tableMeet.setModel(model);
	}

	/**
	 * Create the dialog.
	 * @param dataTable le contenu de la table
	 * @param columnsTable le nom des colonnes de la table
	 */
	public MeetingSearch(String[][] dataTable, String[] columnsTable) {
		setTitle("Rendez-vous - Rechercher");
		setModal(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(0, 0, 434, 228);
			contentPanel.add(scrollPane);
			{
				tableMeet = new JTable();
				tableMeet.setEnabled(false);
				setTable(dataTable, columnsTable);
				scrollPane.setViewportView(tableMeet);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton btnFermer = new JButton("Fermer");
				btnFermer.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				buttonPane.add(btnFermer);
			}
		}
	}

	@Override
	public void assignListener(Ctrl ctrl) {
		MeetingSearch.tableMeet.addMouseListener(ctrl);	
	}
}
