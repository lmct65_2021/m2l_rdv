package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Classe d�finissant la vue g�n�rale de l'application
 * @author xavier
 *
 */
public class MainView extends JFrame implements ViewM2L{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnRDV;

	/**
	 * Launch the application.
	 * @param args arguments du main
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//Cr�ation du contr�leur
					Ctrl ctrl = new Ctrl();
					//Cr�ation de la vue g�n�rale
					MainView frame = new MainView();
					//Assignation d'un observateur sur cette vue
					frame.assignListener(ctrl);
					//Affichage de la vue
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainView() {
		setTitle("M2L_RDV - Accueil");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnRDV = new JButton("Gestion des Rendez-vous");
		btnRDV.setBounds(54, 56, 309, 23);
		contentPane.add(btnRDV);
		
		JButton btnFermer = new JButton("Quitter");
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnFermer.setBounds(335, 227, 89, 23);
		contentPane.add(btnFermer);
	}

	@Override
	public void assignListener(Ctrl ctrl) {
		this.btnRDV.setActionCommand("MainView_rdv");
		this.btnRDV.addActionListener(ctrl);		
	}
}
