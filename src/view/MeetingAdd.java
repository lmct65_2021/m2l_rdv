package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;

/**
 * Classe d�finissant la vue d'ajout d'un rendez-vous
 * @author xavier
 *
 */
public class MeetingAdd extends JDialog implements ViewM2L{

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JButton btnValider;
	private JButton btnAnnuler;
	private static JTextField txtNom;
	private static JComboBox<String> cbxLigues;
	private static JTextField txtDate;

	/**
	 * M�thode statique permettant de r�initialiser les champs
	 */
	public static void init(){
		txtDate.setText("");
		txtNom.setText("");
	}
	
	/**
	 * M�thode statique permettant d'obtenir le contenu du champ texte nom
	 * @return le contenu du champ texte nom
	 */
	public static String getTxtName(){
		return txtNom.getText();
	}
	
	/**
	 * M�thode statique permettant d'obtenir la s�lection de la liste d�roulante ligues
	 * @return la selection de la liste d�roulante ligues
	 */
	public static String getTxtLeague(){
		return (String) cbxLigues.getSelectedItem();
	}
	
	/**
	 * M�thode statique permettant d'obtenir le contenu du champ texte date
	 * @return le contenu du champ texte date
	 */
	public static String getTxtMeetingDate(){
		if(txtDate.getText().equals(""))
			return null;
		return txtDate.getText();
	}
	/**
	 * M�thode statique permettant de placer le curseur dans le champ texte nom
	 */
	public static void focusTxtName(){
		MeetingAdd.txtNom.requestFocus();
	}
	
	/**
	 * Create the dialog.
	 * @param leagues les ligues � int�grer dans la liste d�roulante
	 */
	public MeetingAdd(String[] leagues) {
		setTitle("Rendez-vous - Ajouter");
		setModal(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblNom = new JLabel("Nom :");
		lblNom.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNom.setBounds(83, 45, 50, 14);
		contentPanel.add(lblNom);
		
		txtNom = new JTextField();
		txtNom.setBounds(140, 42, 192, 20);
		contentPanel.add(txtNom);
		txtNom.setColumns(10);
		
		JLabel lblLigue = new JLabel("Ligue :");
		lblLigue.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLigue.setBounds(63, 128, 70, 14);
		contentPanel.add(lblLigue);
		
		cbxLigues = new JComboBox<String>(leagues);
		//cbxLigues = new JComboBox<String>();
		cbxLigues.setBounds(140, 125, 192, 20);
		contentPanel.add(cbxLigues);
		
		JLabel lblDate = new JLabel("Date :");
		lblDate.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDate.setBounds(53, 87, 80, 14);
		contentPanel.add(lblDate);
		
		txtDate = new JTextField();
		txtDate.setBounds(140, 84, 192, 20);
		contentPanel.add(txtDate);
		txtDate.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				btnValider = new JButton("Valider");
				buttonPane.add(btnValider);
				getRootPane().setDefaultButton(btnValider);
			}
			{
				btnAnnuler = new JButton("Annuler");
				buttonPane.add(btnAnnuler);
			}
			{
				JButton btnFermer = new JButton("Fermer");
				btnFermer.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				buttonPane.add(btnFermer);
			}
		}
	}


	@Override
	public void assignListener(Ctrl ctrl) {
		this.btnValider.setActionCommand("MeetingAdd_valider");
		this.btnValider.addActionListener(ctrl);
		this.btnAnnuler.setActionCommand("MeetingAdd_annuler");
		this.btnAnnuler.addActionListener(ctrl);
	}
}
