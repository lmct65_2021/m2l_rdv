package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Ctrl;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
/**
 * Classe d�finissant la vue de modification d'un rendez-vous
 * @author xavier
 *
 */
public class MeetingChange extends JDialog implements ViewM2L{

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JButton btnValider;
	public static JButton btnAnnuler;
	private static JTextField txtNom;
	private static JComboBox<String> cbxLigues;
	private static JTextField txtDate;
	/**
	 * M�thode statique permettant d'obtenir le contenu du champ texte nom
	 * @return le contenu du champ texte nom
	 */
	public static String getTxtName(){
		return txtNom.getText();
	}
	/**
	 * M�thode statique permettant d'obtenir la s�lection de la liste d�roulante ligue
	 * @return la selection de la liste d�roulante ligue
	 */
	public static String getTxtLeague(){
		return (String) cbxLigues.getSelectedItem();
	}
	/**
	 * M�thode statique permettant d'obtenir le contenu du champ texte date
	 * @return le contenu du champ texte date
	 */
	public static String getTxtMeetingDate(){
		if(txtDate.getText().equals(""))
			return null;
		return txtDate.getText();
	}
	
	/**
	 * Create the dialog.
	 * @param leagues les ligues � int�grer dans la liste d�roulante
	 * @param meeting le d�tail du rendez-vous � modifier
	 */
	public MeetingChange(String[] leagues, String[] meeting) {
		setTitle("Rendez-vous - Modifier");
		setModal(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblNom = new JLabel("Nom :");
		lblNom.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNom.setBounds(83, 45, 50, 14);
		contentPanel.add(lblNom);
		
		txtNom = new JTextField();
		txtNom.setEnabled(false);
		txtNom.setBounds(140, 42, 192, 20);
		contentPanel.add(txtNom);
		txtNom.setColumns(10);
		txtNom.setText(meeting[0]);
		
		JLabel lblLigue = new JLabel("Ligue :");
		lblLigue.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLigue.setBounds(63, 127, 70, 14);
		contentPanel.add(lblLigue);
		
		cbxLigues = new JComboBox<String>(leagues);
		//cbxLigues = new JComboBox<String>();
		cbxLigues.setBounds(140, 124, 192, 20);
		contentPanel.add(cbxLigues);
		cbxLigues.setSelectedItem(meeting[1]);
		
		JLabel lblDate = new JLabel("Date :");
		lblDate.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDate.setBounds(53, 86, 80, 14);
		contentPanel.add(lblDate);
		
		txtDate = new JTextField();
		txtDate.setBounds(140, 83, 192, 20);
		contentPanel.add(txtDate);
		txtDate.setColumns(10);
		txtDate.setText(meeting[2]);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				btnValider = new JButton("Valider");
				buttonPane.add(btnValider);
				getRootPane().setDefaultButton(btnValider);
			}
			{
				btnAnnuler = new JButton("Annuler");
				btnAnnuler.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				buttonPane.add(btnAnnuler);
			}
		}
	}


	@Override
	public void assignListener(Ctrl ctrl) {
		this.btnValider.setActionCommand("MeetingChange_valider");
		this.btnValider.addActionListener(ctrl);
	}
}
