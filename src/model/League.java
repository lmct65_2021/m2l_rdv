package model;

import java.util.ArrayList;
/**
 * Classe d'objet metier LEAGUE
 * @author xavier
 * 
 */
public class League {
	/**
	 * Identifiant de la ligue
	 */
	private int id;
	/**
	 * Nom de la ligue
	 */
	private String name;
	/**
	 * Liste statique de toutes les ligues
	 */
	public static ArrayList<League> allTheLeagues = new ArrayList<League>();
	
	/**
	 * Constructeur de la classe League
	 * @param id identifiant de la nouvelle ligue
	 * @param name nom de la nouvelle ligue
	 */
	public League(int id, String name) {
		super();
		this.id = id;
		this.name = name;
		allTheLeagues.add(this);
	}
	/**
	 * Accesseur en lecture sur l'identifiant de la ligue
	 * @return l'identifiant de la ligue
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * Accesseur en lecture sur le nom de la ligue
	 * @return le nom de la ligue
	 */
	public String getName() {
		return name;
	}
	/**
	 * M�thode permettant de rechercher parmi toutes les ligues
	 * celle ayant un identifiant correspondant � celui pass� en param�tre
	 * @param id l'identifiant � rechercher
	 * @return la League correspondante
	 */
	public static League getLeagueById(int id){
		League found = null;
		for(League l : League.allTheLeagues){
			if(l.getId()==id)
				found=l;
		}
		return found;
	}
	/**
	 * M�thode permettant de rechercher parmi toutes les ligues
	 * celle ayant un nom correspondant � celui pass� en param�tre
	 * @param name le nom � rechercher
	 * @return la League correspondante
	 */
	public static League getLeagueByName(String name) {
		League found = null;
		for(League l : League.allTheLeagues){
			if(l.getName().equals(name))
				found=l;
		}
		return found;
	}

}
