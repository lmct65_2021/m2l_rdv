package model;

import java.util.ArrayList;
import java.util.GregorianCalendar;
/**
 * Classe d'objet metier MEETING
 * @author xavier
 *
 */
public class Meeting {
	/**
	 * Le nom du rendez-vous
	 */
	private String name;
	/**
	 * La ligue du rendez-vous
	 */
	private League itsLeague;
	/**
	 * Date du rendez-vous
	 */
	private GregorianCalendar meetingDate;
	/**
	 * Liste statique de tous les rendez-vous
	 */
	public static ArrayList<Meeting> allTheMeetings = new ArrayList<Meeting>();
	
	/**
	 * Construcuteur de la classe Meeting
	 * @param name nom de la personne du nouveau rendez-vous
	 * @param itsLeague ligue du nouveau rendez-vous
	 * @param meetingDate date du nouveau rendez-vous
	 */
	public Meeting(String name, League itsLeague, GregorianCalendar meetingDate) {
		super();
		this.name = name;
		this.itsLeague = itsLeague;
		this.meetingDate = meetingDate;
		allTheMeetings.add(this);
	}

	/**
	 * Accesseur en lecture sur le nom du rendez-vous
	 * @return le nom du rendez-vous
	 */
	public String getName() {
		return name;
	}

	/**
	 * Accesseur en lecture sur la ligue du rendez-vous
	 * @return la ligue du rendez-vous
	 */
	public League getItsLeague() {
		return itsLeague;
	}

	/**
	 * Accesseur en lecture sur la date du rendez-vous
	 * @return la date du rendez-vous
	 */
	public GregorianCalendar getMeetingDate() {
		return meetingDate;
	}
	
	/**
	 * M�thode permettant de rechercher parmi tous les rendez-vous
	 * celui ayant un nom correspondant � celui pass� en param�tre
	 * @param name le nom � rechercher
	 * @return le Meeting correspondant
	 */
	public static Meeting getMeetingByName(String name) {
		Meeting found = null;
		for(Meeting m : Meeting.allTheMeetings){
			if(m.getName().equals(name))
				found=m;
		}
		return found;
	}

	/**
	 * Accesseur en �criture sur la ligue du rendez-vous
	 * @param itsLeague la nouvelle ligue du rendez-vous
	 */
	public void setItsLeague(League itsLeague) {
		this.itsLeague = itsLeague;
	}

	/**
	 * Accesseur en �criture sur la date du rendez-vous
	 * @param meetingDate la nouvelle date du rendez-vous
	 */
	public void setMeetingDate(GregorianCalendar meetingDate) {
		this.meetingDate = meetingDate;
	}

	
}
