package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import library.DatesConverter;
import library.Persistence;
import model.League;
import model.Meeting;
import view.MeetingAdd;
import view.MeetingChange;
import view.MeetingHome;
import view.MeetingSearch;
/**
 * Classe CONTROLEUR
 * @author xavier
 *
 */
public class Ctrl implements ActionListener, MouseListener{
	
	/**
	 * Constructeur de la classe Ctrl
	 * Ce constructeur permet, en plus de cr�er une instance de Ctrl, de cr�er tous les objets de l'application � partir de la base de donn�es
	 */
	public Ctrl(){
		//Cr�ation des objets League
		String[][] dataLeague = null;
		try {
			dataLeague = Persistence.load("ligue");
		} catch (SQLException e) {
			String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
			JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
		}
		for(int i=0;i<dataLeague.length;i++){
			new League(Integer.parseInt(dataLeague[i][0]),dataLeague[i][1]);
		}
		//Cr�ation des objets Meeting
		String[][] dataMeeting = null;
		try {
			dataMeeting = Persistence.load("rendezvous");
		} catch (SQLException e) {
			String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
			JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
		}
		for(int i=0;i<dataMeeting.length;i++){
			new Meeting(dataMeeting[i][1],League.getLeagueById(Integer.parseInt(dataMeeting[i][2])),DatesConverter.USStringToDate(dataMeeting[i][3]));
		}
	}

	/**
	 * M�thode d�clench�e lors de clics sur les boutons de l'application
	 */
	@Override
	public void actionPerformed(ActionEvent evt) {
		//R�cup�ration de l'action
		String action = evt.getActionCommand();
		//D�coupage et analyse de celle-ci
		String details[] = action.split("_");
		//who : QUI ? Quelle vue a effectu� l'action ?
		String who = details[0];
		//what : QUOI ? Qu'est-ce-que cette vue souhaite faire ?
		String what = details[1];
		//switch-case de traitement des diff�rents cas
		switch(who){
		case "MainView":
			switch(what){
			case "export":
				break;
			case "rdv":
				//Cr�ation de la vue d'accueil des ligues
				MeetingHome frame = new MeetingHome();
				//Assignation d'un observateur sur cette vue
				frame.assignListener(this);
				//Affichage de la vue
				frame.setVisible(true);
				break;
			}
			break;
		case "MeetingHome":
			switch(what){
			case "ajout":
				//Cr�ation de la vue d'ajout d'une ligue
				MeetingAdd frame = new MeetingAdd(this.leaguesBox());
				//Assignation d'un observateur sur cette vue
				frame.assignListener(this);
				//Affichage de la vue
				frame.setVisible(true);
				break;
			case "rechercherModifier":				
				String[][] dataTable = this.meetingTable();
				String[] dataColumns = {"Nom","Ligue","Date"};
				//Cr�ation de la vue de recherche d'un rendez-vous
				MeetingSearch frame1 = new MeetingSearch(dataTable,dataColumns);
				//Assignation d'un observateur sur cette vue
				frame1.assignListener(this);
				//Affichage de la vue
				frame1.setVisible(true);
				break;
			}
			break;
		case "MeetingAdd":
			switch(what){
			case "valider":
				//R�cup�ration des informations saisies par l'utilisateur
				String nomRdv = MeetingAdd.getTxtName();
				if(nomRdv.equals("")){
					JOptionPane.showMessageDialog(null,"Le nom du rendez-vous � �t� omis. Veillez � le saisir correctement.","Erreur Saisie",JOptionPane.WARNING_MESSAGE);
					MeetingAdd.focusTxtName();
				}
				else{
					String nomL = MeetingAdd.getTxtLeague();
					League ligueRdv = League.getLeagueByName(nomL);
					String dateRdv = MeetingAdd.getTxtMeetingDate();
					//Cr�ation du nouvel objet Meeting
					Meeting meet = new Meeting(nomRdv,ligueRdv,DatesConverter.FRStringToDate(dateRdv));
					//INSERT dans la BD
					try {
						Persistence.insertMeeting(meet.getName(),meet.getItsLeague().getId(),meet.getMeetingDate());
						//Message de confirmation pour l'utilisateur
						JOptionPane.showMessageDialog(null,"Le rendez-vous a bien �t� ajout�","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
						//R�initialisation des champs
						MeetingAdd.init();
					} catch (SQLException e) {
						String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
						JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
					}
				}
				break;
			case "annuler":
				//R�initialisation des champs
				MeetingAdd.init();
				break;
			}
			break;
		case "MeetingSearch":
				break;
		case "MeetingChange":
			switch(what){
			case "valider":
				//R�cup�ration des informations saisies par l'utilisateur
				String nom = MeetingChange.getTxtName();
				String nomL = MeetingChange.getTxtLeague();
				League ligue = League.getLeagueByName(nomL);
				String dateRdv = MeetingChange.getTxtMeetingDate();
				//R�cup�ration de l'objet Medicine � modifier
				Meeting meet = Meeting.getMeetingByName(nom);
				//Modification de celui-ci � travers les setteurs
				meet.setItsLeague(ligue);
				meet.setMeetingDate(DatesConverter.FRStringToDate(dateRdv));
				//UPDATE dans la BD
				try {
					Persistence.updateMeeting(meet.getName(),meet.getItsLeague().getId(),meet.getMeetingDate());
					//Mise � jour de la jtable
					String[][] dataTable = this.meetingTable();
					String[] dataColumns = {"Nom","Ligue","Date"};
					MeetingSearch.setTable(dataTable, dataColumns);
					//Modification du bouton (annuler devient fermer)
					MeetingChange.btnAnnuler.setText("Fermer");
					//Message de confirmation pour l'utilisateur
					JOptionPane.showMessageDialog(null,"Le rendez-vous a bien �t� modifi�","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);					
				} catch (SQLException e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				break;
			}
			break;
		}	
	}

	/**
	 * M�thode permettant d'interroger le mod�le afin de construire un tableau contenant tous les rendez-vous
	 * @return un tableau � deux dimensions contenant tous les rendez-vous (nom,idLigue,dateRdV)
	 */
	private String[][] meetingTable() {
		int i=0;
		String[][] liste=new String[Meeting.allTheMeetings.size()][3];
		for(Meeting m : Meeting.allTheMeetings){
			liste[i][0]=m.getName();
			liste[i][1]=m.getItsLeague().getName();
			liste[i][2]=DatesConverter.dateToStringFR(m.getMeetingDate());
			i++;
		}
		return liste;
	}

	/**
	 * M�thode permettant d'interroger le mod�le afin de construire un tableau contenant toutes les ligues
	 * @return un tableau � une dimension contenant toutes les ligues (nom)
	 */
	private String[] leaguesBox(){
		int i=0;
		String[] liste=new String[League.allTheLeagues.size()];
		for(League l : League.allTheLeagues){
			liste[i]=l.getName();
			i++;
		}
		return liste;
	}

	/**
	 * M�thode d�clanch�e lors de clics souris sur l'application
	 */
	@Override
	public void mouseClicked(MouseEvent evt) {
		//S'il s'agit d'un double-clic
		if(evt.getClickCount()==2){
			//R�cup�ration de la jtable dans laquelle l'utilisateur a double-cliqu�
			JTable laTable = (JTable)evt.getComponent();
			//R�cup�ration du num�ro de la ligne de cette jtable sur laquelle il a double-cliqu�
			int row=laTable.rowAtPoint(evt.getPoint());
			//R�cup�ration du rendez-vous � partir de ces informations
			Meeting meet = Meeting.getMeetingByName(laTable.getValueAt(row,0).toString());
			//Cr�ation d'un tableau contenant le d�tail du rendez-vous
			String[] data = new String[3];
			data[0]=meet.getName();
			data[1]=meet.getItsLeague().getName();
			data[2]=DatesConverter.dateToStringFR(meet.getMeetingDate());
			//Cr�ation de la vue de modification du rendez-vous s�lectionn� dans la jtable
			MeetingChange frame = new MeetingChange(this.leaguesBox(),data);
			//Assignation d'un observateur sur cette vue
			frame.assignListener(this);
			//Affichage de la vue
			frame.setVisible(true);
		 } 
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
