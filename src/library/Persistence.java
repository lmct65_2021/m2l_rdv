package library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.GregorianCalendar;
/**
 * Classe de persistance des objets dans une base SQL
 * @author xavier
 *
 */
public abstract class Persistence {
	
	/**
	 * M�thode d'INSERT d'un nouveau rendez-vous
	 * @param name le nom du nouveau rendez-vous
	 * @param idLeague l'identifiant de la ligue du nouveau rendez-vous
	 * @param meetingDate la date du nouveau rendez-vous
	 * @throws SQLException l'exception SQL lev�e
	 */
	public static void insertMeeting(String name, int idLeague, GregorianCalendar meetingDate) throws SQLException{
		Connection cn = Persistence.connection();
		Statement stmt;
		try{
			 stmt = cn.createStatement();
			 if(meetingDate!=null)
				 stmt.executeUpdate("INSERT INTO rendezvous (nom,idLigue,dateRdv) VALUES ('"+name+"',"+idLeague+",'"+DatesConverter.dateToStringFR(meetingDate)+"')");
			 else
				 stmt.executeUpdate("INSERT INTO rendezvous (nom,idLigue,dateRdv) VALUES ('"+name+"',"+idLeague+",null)");
		}catch (SQLException e){
			throw e;
		}
		finally{
			Persistence.closeConnection(cn);
		}
	}
	/**
	 * M�thode d'INSERT d'une nouvelle ligue
	 * @param name le nom de la nouvelle ligue
	 * @throws SQLException l'exception SQL lev�e
	 */
	public static void insertLeague(String name) throws SQLException{
		Connection cn = Persistence.connection();
		Statement stmt;
		
		try {
			 stmt = cn.createStatement();
			stmt.executeUpdate("INSERT INTO ligue (nom) VALUES ('"+name+"')");
		} catch (SQLException e) {
			throw e;
		}
		finally{
			Persistence.closeConnection(cn);
		}
	}

	/**
	 * M�thode de SELECT des tables
	 * @param table le nom de la table SQL � s�lectionner
	 * @return un tableau � deux dimensions contenant tous les tuples de la table
	 * @throws SQLException l'exception SQL lev�e
	 */
	public static String[][] load(String table) throws SQLException{	
		//D�claration des variables
		Connection cn = Persistence.connection();
		Statement stmt; 
		ResultSet rs = null;
		ResultSetMetaData metadata;
		int rowCount,columnCount,rowNum;
		String columnName;
		String[][] result = null;
		
	    try 
	    {
	    	stmt= cn.createStatement();
	    	//D�finition de la requete pour construire le jeu d'enregistrement
	    	rs = stmt.executeQuery("SELECT count(*) FROM "+table);
			//R�cup�ration du nombre de lignes du jeu d'enregistrement
	    	rs.next();
			rowCount=rs.getInt(1);
	    	//D�finition de la requete pour construire le jeu d'enregistrement
	    	rs = stmt.executeQuery("SELECT * FROM "+table);
			metadata = rs.getMetaData();
			//R�cup�ration du nombre de colonnes du jeu d'enregistrement
			columnCount = metadata.getColumnCount();
			//D�claration du tableau qui contiendra toutes les informations
			result = new String[rowCount][columnCount];
			//PArcours du jeu d'enregistrement
			rowNum = 0;
	        while (rs.next()) 
	        {
	        	for (int numCol=0; numCol<columnCount; numCol++)
	        	{
	        		//Insertion de la valeur dans une case du tableau
	        		columnName = metadata.getColumnName(numCol+1);
		        	result[rowNum][numCol] = rs.getString(columnName);
	        	}
	        	rowNum++;
	        }
	        
		} catch (SQLException e) 
		{
			throw e;
		}
	    finally{
	    	Persistence.closeConnection(cn);
	    }
	return result;
	}

	/**
	 * M�thode de connexion � la BD
	 * @return une connexion active sur la BD
	 * @throws SQLException l'exception SQL lev�e
	 */
	private static Connection connection() throws SQLException{
		String host = "192.168.1.28";
		String base = "m2lrdv";
		String user = "lmct65";
		String passwd = "lmct65";
		Connection conn = null;
		try
		{
			String connectionString ="jdbc:sqlserver://"+host+";database="+base+";user="+user+";password="+passwd;
			conn = DriverManager.getConnection(connectionString);
		}
		catch (SQLException e) 
		{
			throw e;
		}
		return conn; 
	}
	
	/**
	 * M�thode de cl�ture de connexion
	 * @param conn la connexion � fermer
	 * @throws SQLException l'exception SQL lev�e
	 */
	private static void closeConnection(Connection conn) throws SQLException{
		try {
			conn.close();
		} catch (SQLException e) {
			throw e;
		}
	}

	/**
	 * M�thode d'UPDATE d'un rendez-vous
	 * @param name le nom du rendez-vous � modifier
	 * @param idLeague la nouvelle ligue du rendez-vous � modifier
	 * @param MeetingDate la nouvelle date du rendez-vous � modifier
	 * @throws SQLException l'exception SQL lev�e
	 */
	public static void updateMeeting(String name, int idLeague, GregorianCalendar MeetingDate) throws SQLException {
		Connection cn = Persistence.connection();
		Statement stmt;
		try{
			 stmt = cn.createStatement();
			 stmt.executeUpdate("UPDATE rendezvous SET idLigue="+idLeague+" WHERE nom='"+name+"'");
			 if(MeetingDate!=null)
				 stmt.executeUpdate("UPDATE rendezvous SET dateRdv='"+DatesConverter.dateToStringFR(MeetingDate)+"' WHERE nom='"+name+"'");
		}catch (SQLException e){
			throw e;
		}
		finally{
			Persistence.closeConnection(cn);
		}
	}

}
