
-- ligue definition

-- Drop table

-- DROP TABLE ligue GO

CREATE TABLE ligue (
	id int IDENTITY(1,1) NOT NULL,
	nom varchar(20) NULL,
	CONSTRAINT pk_ligue PRIMARY KEY (id)
) GO;


-- rendezvous definition

-- Drop table

-- DROP TABLE rendezvous GO

CREATE TABLE rendezvous (
	id int IDENTITY(1,1) NOT NULL,
	nom varchar(50) NULL,
	idLigue int NULL,
	dateRdv datetime NULL,
	CONSTRAINT pk_rendezvous PRIMARY KEY (id),
	CONSTRAINT fk_rendezvous_ligue FOREIGN KEY (idLigue) REFERENCES ligue(id)
) GO;


